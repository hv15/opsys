#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define GB 1073741824 /* 1GB in Bytes */
#define KB 1024 /* 1KB in bytes */

int main(int argc, char ** argv){

	const size_t max_size = 0.95 * GB;
	static size_t size = KB;
	FILE * file = fopen("output.txt", "w");
	if(file == NULL)
	{
		perror("Couldn't open file");
		return EXIT_FAILURE;
	}

	void * pointer;
	int count = 1;

	void * prt1 = malloc(size);
	fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, prt1);
	void * prt2 = malloc(size);
	fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, prt2);
	void * prt3 = malloc(size);
	fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, prt3);

	while(size < max_size){
		pointer = malloc(size);

		if(pointer == NULL)
		{
			fprintf(file, "Malloc finished at %p with size %lu bytes\n", pointer, size);
			printf("Malloc finished at %p with size %lu bytes\n", pointer, size);
			break;
		}

		fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, pointer);
		//printf("STEP:%-12lu ADDRESS:%-12p\n", size, pointer);

		free(pointer);
		fprintf(file, "FREEING %p\n", pointer);

		size += (KB * count);
		count += 1;

		fflush(file);
	}

	free(prt1);
	fprintf(file, "FREEING %p\n", prt1);
	free(prt2);
	fprintf(file, "FREEING %p\n", prt2);
	free(prt3);
	fprintf(file, "FREEING %p\n", prt3);

	void * prt4 = malloc(KB);
	fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, prt4);
	void * prt5 = malloc(KB*3);
	fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, prt5);
	void * prt6 = malloc(KB*5);
	fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, prt6);
	
	fclose(file);

	return EXIT_SUCCESS;
}
