#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_malloc.h"

#define FREE 0
#define USED 1
#define GB 1073741824 /* 1GB in Bytes */
#define MB 104857600 /* 100MB in Bytes */

struct chunk_info {
	size_t size;
	short on;
	struct chunk_info * next;
	struct chunk_info * prev;
	struct chunk_info * nextFree;
	struct chunk_info * prevFree;
};

static struct chunk_info * firstFree = NULL;

static size_t chunk_size = sizeof(struct chunk_info);

struct chunk_info * incPointer(struct chunk_info *pointer, size_t val){
	return (struct chunk_info *)((size_t)pointer + val);
}

void * initMalloc(size_t size) {
	// In case we are back at the start, reset the firstFree
	firstFree = NULL;

	// TDDDDDFXXXXXXXX
	// |
	struct chunk_info * start = (struct chunk_info *) sbrk(GB);
	
	// TDDDDDFXXXXXXXX
	//       |
	firstFree = incPointer(start, (size + chunk_size));
	
	// TDDDDDF<XXXXXXX>
	firstFree->size = GB - size - (2*chunk_size);
	firstFree->on = FREE;
	firstFree->next = NULL;
	firstFree->prev = start;
	firstFree->nextFree = NULL;
	firstFree->prevFree = NULL;
	
	// T<DDDDD>FXXXXXXX
	start->size = size;
	start->on = USED;
	start->prev = NULL;
	start->next = firstFree;
	start->nextFree = NULL;
	start->prevFree = NULL;
	
	// TDDDDDFXXXXXXXX
	//  |
	return incPointer(start, chunk_size);
}

void * newEnd(struct chunk_info * ci, size_t size) {

	if(ci->size < (size + chunk_size)) return NULL;
	
	// TDDDDDEXXXXXXXX
	//       |   
	struct chunk_info * newEnd = incPointer(ci, (chunk_size + size));
	
	// TDDDDDE<XXXXXXXX>
	newEnd->size = ci->size - size - chunk_size;
	newEnd->on = FREE;
	newEnd->next = NULL;
	newEnd->prev = ci;
	newEnd->nextFree = NULL;
	if(ci->prevFree != NULL) {
		newEnd->prevFree = ci->prevFree;
		//ci->prevFree->nextFree = newEnd;
	}
	else firstFree = newEnd;
	
	// TDDDDDC<DDDDDDD>EXXXXXXX
	ci->size = size;
	if(ci->prevFree != NULL) ci->prevFree->nextFree = newEnd;
	
	// TDDDDDC<DDDDDDD>EXXXXXXX
	//                 |
	ci->next = newEnd;
	//printf("p:%p\n", (void*)ci);
	
	// TDDDDDCDDDDDDDEXXXXXXX
	//        |
	return incPointer(ci, chunk_size);
}

void * my_malloc(size_t size) {
	
	if(firstFree == NULL) return initMalloc(size);
	
	struct chunk_info * ci = firstFree;

	while(1) {
		// we lose some size, each time we malloc.....
		if(ci->size >= size){
			ci->on = USED;
			// if we are taking from the firstFree chunk, why would it have nextFree?
			if(ci->prevFree != NULL && ci->nextFree != NULL) ci->prevFree = ci->nextFree;
			else if(ci->nextFree != NULL) firstFree = ci->nextFree;
			else return newEnd(ci, size);
			
			// TDDDDDIDDDDDDEXXXXXX
			//        |
			return incPointer(ci, chunk_size);
		}
		if(ci->nextFree == NULL) break;
		ci = ci->nextFree;
		//printf("%p -> size: %lu need size:%d\n", (void *) ci, ci->size, size);
	}
	return NULL;
}

/* Our fall thorughs are wrong, as can be seen below */
void my_free(void * object) {
	// TDDDDDIXXXXXXXEXXXXXX
	//       |
	struct chunk_info * ci = incPointer(object, -chunk_size);

	if(ci->prev != NULL && ci->prev->on == FREE){
		if(ci->next != NULL) ci->next->prev = ci->prev;
		ci->prev->next = ci->next;
		
		ci->prev->size = ci->prev->size + ci->size + chunk_size;
		ci = ci->prev;
	}
	if(ci->next->next != NULL && ci->next->on == FREE){
		//puts("next");
		if(ci->next->prevFree != NULL) {
			ci->next->prevFree->nextFree = ci;
			ci->prevFree = ci->next->prevFree;
		}
		ci->next->nextFree->prevFree = ci;
		ci->nextFree = ci->next->nextFree;
		
		ci->size = ci->size + ci->next->size + chunk_size;
		ci->next = ci->next->next;
		// this is where Test 3 seg faults at, without the condition
		// though, it exits with 1...so both fail...
		ci->next->next->prev = ci;
	}
	/*
	// this section doesn't make a lot of sense....
	// why are we wanting to keep a node in the front with no size?
	if(ci->next->next == NULL && ci->next->on == FREE){
		puts("hello");
		// why are we doing this?
		struct chunk_info * newCi = incPointer(ci, chunk_size);
		if(firstFree == ci->next) firstFree = newCi;
		newCi->size = ci->size + chunk_size + ci->next->size;
		ci->next = newCi;
		newCi->prev = ci;
		newCi->next = NULL;
		
		newCi->nextFree = NULL;
		if(ci->prevFree != NULL) ci->prevFree->nextFree = newCi;
		
		
		ci->size = 0;
		ci->on = FREE;
		ci = newCi;
		return;
	}*/
	ci->on = FREE;
	
	firstFree->prevFree = ci;
	if(ci != firstFree) ci->nextFree = firstFree;
	else ci->nextFree = firstFree->nextFree;
	if(ci->prevFree != NULL) ci->prevFree->nextFree = ci->nextFree;
	ci->nextFree->prevFree = ci->prevFree;
	ci->prevFree = NULL;
	
	firstFree = ci;
	
	// This my solution for when there is only one 1 used node and the firstFree node
	// this seems to fix the the merge issue we where having
	if(ci->next != NULL && ci->next->on == FREE){
		puts("head");
		if(ci == firstFree){
			ci->size = GB - chunk_size;
		}
		else {
			ci->size = ci->next->size + ci->size + chunk_size;
			//printf("NO-HEAD ci:%p ci->nextFree:%p ci->prevFree:%p, ci->size:%lu firstFree:%p\n", (void *) ci, (void *) ci->nextFree, (void *) ci->prevFree, ci->size, (void *) firstFree);
		}
		ci->on = FREE;
		ci->next = NULL;
		ci->prev = NULL;
		ci->nextFree = NULL;
		ci->prevFree = NULL;
	}

	//printf("AFTER ci:%p ci->nextFree:%p ci->prevFree:%p, firstFree:%p\n", (void *) ci, (void *) ci->nextFree, (void *) ci->prevFree, (void *) firstFree);
}


void printFrees(){
	struct chunk_info * ci = firstFree;
	printf("FirstFree %p, size: %lu\n", (void *)ci, ci->size);
	while(ci->nextFree != NULL){
		ci = ci->nextFree;
		printf("Free %p, size: %lu\n", (void *)ci, ci->size);
	}
}
	
