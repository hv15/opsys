#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define GB 1073741824 /* 1GB in Bytes */
#define KB 1024 /* 1KB in bytes */

int main(int argc, char ** argv){

	const size_t max_size = 0.95 * GB;
	static size_t size = KB;
	FILE * file = fopen("output.txt", "w");
	if(file == NULL)
	{
		perror("Couldn't open file");
		return EXIT_FAILURE;
	}

	void * pointer;
	int count = 1;

	while(size < max_size){
		pointer = malloc(size);

		if(pointer == NULL)
		{
			fprintf(file, "Malloc failed at %p with size %lu bytes\n", pointer, size);
			printf("Malloc failed at %p with size %lu bytes\n", pointer, size);
			fclose(file);
			return EXIT_FAILURE;
		}

		fprintf(file, "ALLOC-SIZE: %-12lu ADDRESS: %-12p\n", size, pointer);
		//printf("STEP:%-12lu ADDRESS:%-12p\n", size, pointer);

		free(pointer);

		size += (KB * count);
		count += 1;

		fflush(file);
	}

	fclose(file);

	return EXIT_SUCCESS;
}
