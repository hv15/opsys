#include <unistd.h>
void * getNew(size_t size);
void * my_malloc(size_t size);
void my_free(void * object);
