#include <unistd.h>
#include <stdio.h>
#define GB 1073741824 /* 1GB in Bytes */
struct chunk_info {
	size_t size;
	int on;
	struct chunk_info * next;
};

static struct chunk_info * start = NULL;

size_t chunk_size = sizeof(struct chunk_info);

struct chunk_info * incPointer(struct chunk_info *pointer, size_t val){
	return (struct chunk_info *)((size_t)pointer + val);
}

void * initMalloc(size_t size) {
	
	// TDDDDDHDDDDDDDD
	// |
	start = (struct chunk_info *) sbrk(GB);
	
	
	// TDDDDDHDDDDDDDD
	//       |
	struct chunk_info * next  = incPointer(start, (size + chunk_size));
	
	// TDDDDDH<DDDDDDDD>
	next->size = GB - size - (2*chunk_size);
	next->on = 0;
	next->next = NULL;
	
	// T<DDDDD>HDDDDDDDD
	start->size = size;
	start->on = 1;
	start->next = next;
	
	// TDDDDDHDDDDDDDD
	//  |
	return incPointer(start, chunk_size);
}

void * my_malloc(size_t size) {

	if(start == NULL) return initMalloc(size);
	
	struct chunk_info *ci = start;
	while(ci->next != NULL) {
		if(ci->on == 0 && ci->size >= size){
			ci->on = 1;
			// TDDDDDIXXXXXXHHDDDDDDDDD
			//       |
			return incPointer(ci, chunk_size);
		}
		ci = ci->next;
	}
	if(ci->size < (size + chunk_size)) return NULL;
	
	// TDDDDDHDDDDDDDD
	//       |   
	struct chunk_info * next = incPointer(ci, (chunk_size + size));
	
	// TDDDDDH<DDDDDDDD>
	next->size = ci->size - size - chunk_size;
	next->on = 0;
	next->next = NULL;
	
	// TDDDC<DDDDDDD>HDDDDDDDD
	ci->size = size;
	ci->on = 1;
	
	// TDDDCDDDDDDDHDDDDDDDD
	//             |
	ci->next = next;
	
	// TDDDCDDDDDDDHDDDDDDDD
	//      |
	return incPointer(ci, chunk_size);
}


void my_free(void * object) {
	// TDDDCDDDDDDDHDDDDDDDD
	//     |
	struct chunk_info * ci = incPointer(object, -chunk_size);
	ci->on = 0;
	
}
