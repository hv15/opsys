my_malloc/my_free vs malloc/free:

files: malloc-test.txt my_malloc-test.txt

Our malloc implementation (my_malloc) seems to work very similarly to glibc's
malloc, however the most notable difference is that of dynamic space allocation.

glibc's malloc has no problems with acquiring more space, unlike our
implementation, which, having reached the limit of our sbrk chunk, returns null
thus ending the loop prematurely. 

Interestingly, our implementation is similar to glibc's malloc in that it
stores chunk info, such as data size and a flag indicating if it is in use or
free. Thus when free chunks, in both implementations, one can reuse chunks
based solely on chunk sizes.
