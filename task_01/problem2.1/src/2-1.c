#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <syslog.h>
#include <unistd.h>

/*

prints and logs the current time in 10 second intervals forever.

*/

int main(){
	//time variable
	time_t rawtime;
	//infinte loop
	while(1){
		//get current time
		time(&rawtime);
		//print time to stdout
		printf("%s", ctime(&rawtime));
		//log time using syslog
		syslog(LOG_INFO, ctime(&rawtime));
		//wait 10 seconds
		sleep(10);
	}
	//make compiler happy
	return 0;	
}
