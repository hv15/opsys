#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#include "fileLoader.h"

//Counts the newLine characters returns true if it is less than the
//limit and false if larger.
bool count_nl(char* array, int limit){
	char c = array[0];
	char nl = '\n';
	int count = 0;
	int ncount = 0;
	while(c != '\0' && ncount <= limit+1){
		c = array[count++];
		if(c == nl) ncount++;
	}
	return ncount <= limit;
}

/*
 * Creates a child process piping the stdout of the child process into
 * a character buffer. The new lines in this buffer are counted and the
 * program either prints the buffer or a warning message.
 * 
*/
int main(int argc, char** argv){
	int fd[2];
	pid_t ls_proc;
	char* buf;
	
	//We need to know what directory to look at
	if(argc == 1){
		puts("No Arguments given, exiting!");
		return EXIT_FAILURE;
	}

	argv[0] = "-1"; // ensure that ls prints only the filename per row

	pipe(fd); //start the pipe
	ls_proc = fork(); //Call fork to create the child process
	
	if(ls_proc >= 0){
		if(ls_proc == 0){ //This is the child process
			close(fd[0]); //Close the READ pipe
			close(1); //Close STDOUT
			dup2(fd[1],1); //Dump STDOUT to the WRITE pipe
			execv("/bin/ls", argv); //Execute ls with argv

		} else { //This is the parent process
			wait(&ls_proc); //Wait for child process to print to STDOUT (pipe)
			close(fd[1]); //Close the WRITE pipe
			close(0); //Close STDIN
			buf = loadFile(fd[0]); //Passes the READ end of the pipe to loadFile
			if(count_nl(buf, 25)) puts(buf);
			else {
				/*
				 * This uses 'popen' which creates a pipe and forks to create
				 * a 'less' process. The 'less' process is a child of 
				 * this process and essentially share both its stdin and
				 * stdout (via automatically created pipes). Because we
				 * call 'popen' with the 'w' the 'FILE' returned is
				 * piped to the stdin of the 'less' process.
				 * 
				 * 
				 * We then write the contents of buf to the less FILE
				 * using fprintf and close the 'FILE' using 'pclose'. 
				*/
				FILE* less = popen("/usr/bin/less", "w");
				fprintf(less, "%s", buf);
				pclose(less);
			}
		}
	}
	//Make compiler happy
	return EXIT_SUCCESS;
}
