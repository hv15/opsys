#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <syslog.h>
#include <unistd.h>

int main(){
	pid_t childPID;
	char * path = NULL;

	path = getcwd(path,0);
	strcat(path,"/out.txt");
	
	childPID = fork();
	
	if(childPID >= 0){
		if(childPID == 0){
			//In child process
			//time variable
			time_t rawtime;
			//Create a file pointer
			FILE * out;
			//Open the file "out.txt"
			out = fopen(path, "w");
			//infinte loop
			while(1){
				//get current time
				time(&rawtime);
				//print time to file "out.txt" so we can read its output
				fputs(ctime(&rawtime), out);
				//Write to the file *now* rather than on fclose
				fflush(out);
				//log time using syslog (no point really as we can't read it)
				syslog(LOG_INFO, ctime(&rawtime));
				//wait 10 seconds
				sleep(10);
			}
			//make the compiler happier still
			fclose(out);
		} else {
			//In parent process
			//print to confirm creation of child
			printf("%s", "woot");
		}
	}
	//make compiler happy
	return 0;
}
