#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "fileLoader.h"

//Counts the newLine characters returns true if it is less than the
//limit and false if larger.
bool count_nl(char* array, int limit){
	char c = array[0];
	char nl = '\n';
	int count = 0;
	int ncount = 0;
	while(c != '\0' && ncount <= limit+1){
		c = array[count++];
		if(c == nl) ncount++;
	}
	return ncount <= limit;
}
/*
 * Creates a child process piping the stdout of the child process into
 * a character buffer. The new lines in this buffer are counted and the
 * program either prints the buffer or a warning message.
 * 
*/
int main(int argc, char** argv){
	int fd[2];
	pid_t ls_proc;
	char* buf;
	
	//We need to know what directory to look at
	if(argc == 1){
		puts("No Arguments given, exiting!");
		return EXIT_FAILURE;
	}

	argv[0] = "-1"; // ensure that ls prints only one filename per row

	pipe(fd); //create the pipe
	ls_proc = fork(); //Call fork to create the child process
	
	if(ls_proc >= 0){ //Otherwise we have an error
		if(ls_proc == 0){ //This is the child process
			close(fd[0]); //Close the READ end of pipe
			close(1); //Close STDOUT
			dup2(fd[1],1); //Dump STDOUT to the WRITE end of pipe
			execv("/bin/ls", argv); //Execute ls with argv

		} else { //This is the parent process
			wait(&ls_proc); //Wait for child process to print to STDOUT (pipe)
			close(fd[1]); //Close the WRITE end of pipe
			close(0); //Close STDIN
			buf = loadFile(fd[0]); //Passes the READ end of the pipe to loadFile
			if(count_nl(buf, 25)) puts(buf);
			else puts("too many files");
		}
	}
	//Make compiler happy
	return EXIT_SUCCESS;
}
