################################################################################
#
#	AUTHOR: dwhitney67 @ ubuntuforums
#	EDITED:	HANS-NIKOLAI VIESSMANN
#	DATE  : 30/01/13
#
################################################################################

SRCEXT   = c
SRCDIR   = src
OBJDIR   = obj
INCDIR	 = includes

SRCS    := $(shell find $(SRCDIR) -name '*.$(SRCEXT)')
SRCDIRS := $(shell find . -name '*.$(SRCEXT)' -printf '%h ')
OBJS    := $(patsubst %.$(SRCEXT),$(OBJDIR)/%.o,$(SRCS))
APP     := $(shell basename $$PWD)

DEBUG    = -g
INCLUDES = -I$(INCDIR)
ALIAS	 =
CFLAGS   = -Wall -Wextra -pedantic -ansi -D_GNU_SOURCE -D_BSD_SOURCE -c $(DEBUG) $(INCLUDES)
LDFLAGS  = -lpthread -lrt

ifeq ($(SRCEXT), cpp)
CC       = $(CXX)
else
CFLAGS  += -std=c99
endif

.PHONY: all clean distclean

all: $(APP)

$(APP): buildrepo $(OBJS)
	@mkdir -p `dirname $@`
	@echo "Linking $@..."
	@$(CC) $(OBJS) $(LDFLAGS) -o $@
	@echo "The binary is '$@'"

$(OBJDIR)/%.o: %.$(SRCEXT)
	@echo "Generating dependencies for $<..."
	@$(call make-depend,$<,$@,$(subst .o,.d,$@))
	@echo "Compiling $<..."
	@$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) -r $(OBJDIR)

distclean: clean
	$(RM) $(APP)

buildrepo:
	@$(call make-repo)

define make-repo
   	for dir in $(SRCDIRS); \
   	do \
		mkdir -p $(OBJDIR)/$$dir; \
   	done
endef

include $(wildcard *.d)

# usage: $(call make-depend,source-file,object-file,depend-file)
define make-depend
	$(CC) -MM -MF $3 -MP -MT $2 $(CFLAGS) $1
endef
