#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/resource.h>

//Set the default number of threads if the number is not specified as an argument
#define N_THREADS 16

//Set the required constants
#define SIZE 1000
#define ITERATIONS 100000

//The definition of the threadArg type
typedef struct threadArg threadArg_t;

//The struct definition for threadArg objects
struct threadArg {
  pthread_t tid;
  int from;
  int to;
};

//The timespecs for measure runtime
struct timespec start, finish;

//The arrays which hold the data
double a[SIZE];
double b[SIZE];

/* Time difference between a and b in microseconds.  */
int64_t xelapsed (struct timespec a, struct timespec b)
{
    return ((int64_t)a.tv_sec - b.tv_sec) * 1000000
           + ((int64_t)a.tv_nsec - b.tv_nsec) / 1000LL;
}

//Initalize the data in arrays a and b
void initData( )
{
	int i;
	for(i=0; i<SIZE; i++) {
		a[i] = i;
		b[i] = (SIZE-1) - i;
	}
}

//This function is executed in a thread and performs the required operation
//on a selected portion of the array.
void * simd(void *arg)
{
	struct threadArg * ta = (struct threadArg *) arg;

	int i;
	for(i = ta->from; i <= ta->to; i ++){
		a[i] += b[i];
	}

	return NULL;
}

//Calculates the threads required, it then loops for each iteration creating and joining
//a thread for each portion of the array.
void parallel(int threadCount)
{
	struct threadArg threads[threadCount];
	int i, t;
	int threadsUsed;
	for(i = 0; i <= ITERATIONS; i++)
	{
		//Create Threads
		int sliceSize = (SIZE/threadCount)+1;
		for(t = 0; t < threadCount; t ++){

			//Calculate the portion of the array to be processed
			threads[t].from = (t*sliceSize);
			threads[t].to = (threads[t].from + sliceSize);
			
			if(threads[t].from >= SIZE-1) break;
			if(threads[t].to >= SIZE) threads[t].to = SIZE-1;
			
			// if thread creation failed, print an error to STDERR and exit
			if(pthread_create(&(threads[t].tid), NULL, simd, (void *)&threads[t])){
				fprintf(stderr, "Thread %i creation failed", t);
			}
		}
		threadsUsed = t;
		//Join threads
		for(t = 0; t < threadsUsed; t ++){
			pthread_join(threads[t].tid, NULL);
			
		}
	}
}

//The main function gets the number of threads required to process the data
//and then measure the time taken for the operation when executed in parallel.
int main(int argc, char ** args)
{
	/* start the clock */
	clock_gettime(CLOCK_REALTIME, &start);

	initData();
	
	int threadCount;
	if(argc == 2) threadCount = atoi(args[1]);
	else threadCount = N_THREADS;
	parallel(threadCount);
	
	/* finish the clock */
	clock_gettime(CLOCK_REALTIME, &finish);

	printf("Total Running Time: %03li microseconds\n", xelapsed(finish, start));

	return(0);
}
