#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/resource.h>

//Set the default number of threads if the number is not specified as an argument
#define N_THREADS 16

//Set the required constants
#define SIZE 1000
#define ITERATIONS 100000

//The definition of the threadArg type
typedef struct threadArg threadArg_t;

//The struct definition for threadArg objects
struct threadArg {
  pthread_t tid;
  int from;
  int to;
};

//The timespecs for measure runtime
struct timespec start, finish;

//The arrays which hold the data
double a[SIZE];
double b[SIZE];

//The barrier used by the pthreads to synchronize
pthread_barrier_t barrier;

/* Time difference between a and b in microseconds.  */
int64_t xelapsed (struct timespec a, struct timespec b)
{
    return ((int64_t)a.tv_sec - b.tv_sec) * 1000000
           + ((int64_t)a.tv_nsec - b.tv_nsec) / 1000LL;
}

//Initalize the data in arrays a and b
//Also inites the pthread barrier using the threadCount calculated by main
void initData(int threadCount)
{
	int i;
	for( i=0; i<SIZE; i++) {
		a[i] = i;
		b[i] = (SIZE-1) - i;
	}
	pthread_barrier_init(&barrier, NULL, threadCount);
}

//This function is executed in a thread and performs the required operation
//on a selected portion of the array.
//In this case it also does the operation for each iteration. However before
//looping to the next iteration it calls pthread barrier wait. This will synchronise
//all threads to the same iteration.
void * simd(void *arg)
{
	struct threadArg * ta = (struct threadArg *) arg;

	int j, i;
	for(i = 0; i <= ITERATIONS; i++)
	{
		for(j = ta->from; j <= ta->to; j ++){
			a[j] += b[j];
		}
		pthread_barrier_wait(&barrier);
	}
	
	return NULL;
}


//Creates the required threads and calculates the portion of the array they
//must operate on. Once they have completed it then joins all the threads and exits
void parallel(int threadCount)
{
	struct threadArg threads[threadCount];
	int t;
	
	int sliceSize = (SIZE/threadCount)+1;
	
	//Create Threads
	for(t = 0; t < threadCount; t ++){

		threads[t].from = (t*sliceSize);
		threads[t].to = (threads[t].from + sliceSize);
		
		if(threads[t].from >= SIZE-1) break;
		if(threads[t].to >= SIZE) threads[t].to = SIZE-1;
		
		// if thread creation failed, print an error to STDERR and exit
		if(pthread_create(&(threads[t].tid), NULL, simd, (void *)&threads[t])){
			fprintf(stderr, "Thread %i creation failed", t);
		}
	}
	
	//Join the threads once done
	for(t = 0; t < threadCount; t ++){
		pthread_join(threads[t].tid, NULL);
	}
}



int main(int argc, char ** args)
{
	/* start the clock */
	clock_gettime(CLOCK_REALTIME, &start);
	
	int threadCount;
	if(argc == 2) threadCount = atoi(args[1]);
	else threadCount = N_THREADS;
	initData(threadCount);
	
	parallel(threadCount);
	
	/* finish the clock */
	clock_gettime(CLOCK_REALTIME, &finish);

	printf("Total Running Time: %03li microseconds\n", xelapsed(finish, start));

	return(0);
}
