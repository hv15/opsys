#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

#include <sys/time.h>
#include <sys/resource.h>

//Set the required constants
#define SIZE 1000
#define ITERATIONS 100000

//The timespecs for measure runtime
struct timespec start, finish;

//The arrays which hold the data
double a[SIZE];
double b[SIZE];

/* Time difference between a and b in microseconds.  */
int64_t xelapsed (struct timespec a, struct timespec b)
{
    return ((int64_t)a.tv_sec - b.tv_sec) * 1000000
           + ((int64_t)a.tv_nsec - b.tv_nsec) / 1000LL;
}

//Sequentially perform the operations
void sequential( )
{
	int i;
	int g;
	//Loop for each iteration required
	for(i = 0; i < ITERATIONS; i++)
	{
		//Loop over all items of the array performing the required operation
		for(g = 0; g < SIZE; g++)
		{
			a[g] += b[g];
		}
	}
}

//Initalize the data in arrays a and b
void initData( )
{
	int i;
	for(i=0; i<SIZE; i++) {
		a[i] = i;
		b[i] = (SIZE-1) - i;
	}
}

//Run the program first initing the data and running sequentially
int main()
{
	/* start the clock */
	clock_gettime(CLOCK_REALTIME, &start);

	initData();
	sequential();

	/* finish the clock */
	clock_gettime(CLOCK_REALTIME, &finish);

	printf("Total Running Time: %03li microseconds\n", xelapsed(finish, start));

  return(0);
}
