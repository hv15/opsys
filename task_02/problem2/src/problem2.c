#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "search.h"

int main (int argc, char *argv[])
{
	int i, offset;
	const char * term = argv[1];
	FILE* f;
	//Loop over all files to be searched ie. all arguments from the 3rd onwards.
	for(i = 2; i < argc; i ++){
		//open the file to be searched
		f = fopen(argv[i], "r");
		//Call the provided search function
		offset = search(f, term);
		//Check the term was found and print
		if(offset >= 0) printf("%s \toffset=%d\n", argv[i], offset);
		//close the file
		fclose(f);
	}
	return EXIT_SUCCESS;
}

