/* problem3 - search files for given pattern
   Created 2013 by Tomas Roberston and Hans-Nikolai Viessmann
   
   Usage: problem3 [-vh] PATTERN FILE [FILE ...]

     -v[v...]              verbosity level
     -h                    this help message

   Return: 0 on successful termination, -1 otherwise
   
   Detailed description:

     This application takes in a search term [PATTERN] and an
     arbitrary number of filepaths [FILE] and search(s) through
     each FILE looking for a match with PATTERN. If one is found,
     an offset is printed to STDOUT, otherwise it is skipped.

     More specifically, foreach FILE given, the application
     creates a thread which then search through the FILE for the
     given PATTERN.

     Using the verbosity flags, more information can be printed
     to STDOUT.                                                */
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <libgen.h>
#include <pthread.h>

#include "search.h"

// struct containing globally available varibles  
struct global_args {
	int verbose;
	char * pattern;
} args;

// struct containing the file path and thread pseudo-number which is passed to
// searchwrap function.
typedef struct {
	char * file;
	unsigned thread;
} data;

// options string passed to getopts
char * opt_string = "vh";

// function that prints out the help message to STDOUT
void help(char * name){
	printf("  %s - search files for given pattern\n", name);
	printf("  Created 2013 by"
		" Tomas Roberston and"
		" Hans-Nikolai Viessmann\n\n");
	printf("\tusage: %s [-vh] PATTERN FILE [FILE...]\n\n"
		"\t%-15s verbosity level\n"
		"\t%-15s this help message\n\n", name, "-v[v...]", "-h");
	printf("  Return: 0 on successful termination, -1 otherwise\n\n");
	printf("  Detailed Description:\n\n"
		"\tThis application takes in a search term [PATTERN] and an\n"
		"\tarbitrary number of filepaths [FILE] and search(s) through\n"
		"\teach FILE looking for a match with PATTERN. If one is found,\n"
		"\tan offset is printed to STDOUT, otherwise it is skipped.\n\n"
		"\tMore specifically, for each FILE given, the application\n"
		"\tcreates a thread which then search through the FILE for the\n"
		"\tgiven PATTERN.\n\n"
		"\tUsing the verbosity flags, more information can be printed\n"
		"\tto STDOUT.\n");
}

// this function wraps the search function from search.h as to make it work
// with threads. A struct is passed to it containing the filepath and thread
// pseudo-number. The filepath is opened with a read flag set. The search
// function then takes the pattern to search for from the globally available
// struct and search  through the opened filepath. The result is stored in the
// offset variable. The file is then closed. The offset variable is then cast
// as a void pointer and returned.
void * searchwrap(void * array)
{
	// cast the void pointer as struct pointer
	data * in = (data *) array;
	
	// allocate memory to the pointer
	int * offset = malloc(sizeof(int));
	FILE * file;

	// if verbosity flag set, print to STDOUT extra information
	if(args.verbose > 1){
		printf("Thread: %i, looking in"
		" %s for \"%s\"\n", in->thread, in->file, args.pattern);
	}
	
	// open the filepath with the read flag set
	file = fopen(in->file, "r");
	// search through the file for the pattern
	*offset = search(file, args.pattern);

	// close the file
	fclose(file);
	
	// free the memory initial allocated for void pointer
	free(array);
	// return offset cast to void pointer
	return (void *) offset;
}

int main (int argc, char ** argv)
{
	// Initialize local variables
	void * offset;
	int offset_int;
	int i = 0;
	int opt = 0;
	int thread_success = -1;
	struct stat buffer;

	// Initialize the global_args struct
	args.verbose = 0;
	args.pattern = NULL;

	// if nor arguments given, print an error to STDERR and exit
	if(argc == 1){
		fprintf(stderr, "No arguments given\n");
		return EXIT_FAILURE;
	}

	// using getopts, parse each flag:
	// if the verbosity flag is found, increment the verbosity variable
	// in the global_args struct
	// if the help flag is found, call the help function and exit
	// otherwise do nothing
	while((opt = getopt(argc, argv, opt_string)) != -1){
		switch(opt){
			case 'v':
				args.verbose++; // set to true
				break;
			case 'h':
				// pass the name of the binary, i.e. this program
				help(basename(argv[0]));
				return EXIT_SUCCESS;
				break;
			default:
				// nothing happens
				break;
		}
	}

	// Initialize thread important variables
	// get the number of files
	int number = argc - optind - 1;
	pthread_t ids[number];

	// if the number of files is less then 1, print an error to STDERR
	// and exit
	if(number < 1){
		fprintf(stderr, "No files given\n");
		return EXIT_FAILURE;
	}
	
	// set the pattern variable in the global_args struct as the first
	// non-flag argument, i.e. the search term
	args.pattern = argv[optind];

	// for each file, create a thread to search for a match with pattern
	for(i = optind + 1; i < argc; i++){
		
		// ensure that the filepaths given are valid
		if(stat(argv[i], &buffer)){
			fprintf(stderr,
					"Can't access %s: %s\n",
					basename(argv[i]),
					strerror(errno));
			// if a filepath is invalid, set the current thread id to 0
			// and skip to the next iteration of the loop
			ids[i - optind - 1] = 0;
			continue;
		}

		// create the struct that will contain the filepath and
		// thread pseudo-number to be passed to the searchwrap function
		data * array = malloc(sizeof(data));
		array->file = argv[i];
		array->thread = i - optind - 1;

		if(args.verbose > 1){
			printf("%-20s search=\"%s\"\n",
					basename(array->file), args.pattern);
		}

		// create a thread and pass in the data struct as a casted void pointer
		thread_success = pthread_create(&ids[i - optind - 1],
				NULL, searchwrap, (void *) array);

		// if thread creation failed, print an error to STDERR and exit
		if(thread_success){
			fprintf(stderr,
					"Thread %i creation failed: %s", i, strerror(errno));
			return EXIT_FAILURE;
		}

		// set the data struct to null
		array = NULL;
	}

	// for each thread, join it and return the result
	for(i = 0; i < number; i++){

		// if a filepath was invalid for a thread, skip to the next iteration
		// of the loop
		if(ids[i] == 0){
			continue;
		}

		// join the thread back to the process and set offset to the value
		// of the return
		// if the join fails, print an error to STDERR and exit
		if(pthread_join(ids[i], &offset)){
			fprintf(stderr,
					"Thread %i failed to join: %s", i, strerror(errno));
			return EXIT_FAILURE;
		}

		// cast and then dereference offset to get the int value
		offset_int = *(int*)offset;
		// free the allocated memory of offset
		free(offset);

		// print the result to STDOUT
		if(offset_int >= 0){
			printf("%-20s offset=%d\n",
					basename(argv[i + optind + 1]), offset_int);
		} else if(offset_int < 0 && args.verbose > 0) {
			printf("%-20s offset=NONE\n", basename(argv[i + optind + 1]));
		}
	}

	// Happy Days!!!!
	return EXIT_SUCCESS;
}
